# README #

Thanksgiving puzzle solution using OCR implemented and trained from scratch.

### Method ###

Uses HoughCircle transformations for contour discovery, inverted and gaussed pixel sampling for feature detection, and KNearest learning algorithm to achieve 100% read accuracy on any puzzle seen (and solved) in under 10ms.

### To run: ###
install opencv 2.4+ and its python bindings 
run bash script and give picture arg:   ./solve_puzzle.sh puzzle_rearranged4.jpg

### Notes: ###
to generate features:                          python/featureWriter.py
to train, detect, and classify:      python/letterClassification.py


#!/usr/bin/env python
'''
Classifies characters detected in circles, and writes out the graph map to a file
'''
import cv2
import cv2.cv as cv
import numpy as np
import time
import sys

if __name__ == '__main__':
    #read in image, convert to grayscale
    img = cv2.imread(sys.argv[1]);
    print sys.argv[1]
    cimg = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    f = open('../graph_map.txt','w')

    ##set up arrays for training, convert responses to row vector
    samples = np.loadtxt('../trainingData/generalsamples.data',np.float32)
    responses = np.loadtxt('../trainingData/generalresponses.data',np.float32)
    responses = responses.reshape((responses.size,1))


    #train on features with KNearestNeighbors algorithm
    model = cv2.KNearest()
    model.train(samples,responses)


    #find circles in image
    circles = cv2.HoughCircles(cimg,cv2.cv.CV_HOUGH_GRADIENT,1.2,200)#,1,1,param1=200,param2=90,minRadius=0,maxRadius=0,minDist=100)
    circles = np.uint16(np.around(circles))


    loopCnt = 0
    for i in circles[0,:]:


        loopCnt += 1;

        x = i[0]
        y = i[1]
        radius = i[2]
        #cv2.circle(cimg,(i[0],i[1]),2,(255,255,255),3)
        # cv2.imshow('cicr',cimg[y-40:y+40,x-40:x+40])
        imgray = 255 - cimg[y-45:y+45,x-45:x+45]

        #crop and invert
        drawing = 255 - cimg[y-45:y+45,x-45:x+45]

        #resize and find nearest k=1 neighbor
        drawingsmall = cv2.resize(drawing,(10,10))
        drawingsmall = drawingsmall.reshape((1,100))
        drawingsmall = np.float32(drawingsmall)
        retval, results, neigh_resp, dists = model.find_nearest(drawingsmall, k = 1)
        classifiedCharacter = chr(int((results[0][0])))


        #draw out classification indicators
        cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),2)
        cv2.putText(img,str(classifiedCharacter), (i[0]-40,i[1]-40), cv2.FONT_HERSHEY_SIMPLEX, 2, 255)


        #map classified image to puzzle board position
        result = None
        buf = 20
        if((x < 761 + buf and x > 761 - buf) and (y < 674 + buf and y > 674 - buf)):
            result = 'g'
        elif((x < 541 + buf and x > 541 - buf) and (y < 517 + buf and y > 517 - buf)):
            result = 'o'
        elif((x < 541 + buf and x > 541 - buf) and (y < 771 + buf and y > 771 - buf)):
            result = 'w'
        elif((x < 319 + buf and x > 319 - buf) and (y < 365 + buf and y > 365 - buf)):
            result = 't'
        elif((x < 761 + buf and x > 761 - buf) and (y < 358 + buf and y > 358 - buf)):
            result = 'e'
        elif((x < 320 + buf and x > 320 - buf) and (y < 663 + buf and y > 663 - buf)):
            result = 'n'
        elif((x < 539 + buf and x > 539 - buf) and (y < 261 + buf and y > 261 - buf)):
            result = 'l'
        elif((x < 317 + buf and x > 317 - buf) and (y < 925 + buf and y > 925 - buf)):
            result = 's'
        elif((x < 758 + buf and x > 758 - buf) and (y < 935 + buf and y > 935 - buf)):
            result = 'h'
        elif((x < 319 + buf and x > 319 - buf) and (y < 127 + buf and y > 127 - buf)):
            result = 'b'
        elif((x < 97 + buf and x > 97 - buf) and (y < 509 + buf and y > 509 - buf)):
            result = 'y'
        elif((x < 971 + buf and x > 971 - buf) and (y < 523 + buf and y > 523 - buf)):
            result = 'c'
        else:
            result = 'r'

        #print str(loopCnt) + ",(" + str(x) +"," +str(y)+")," + classifiedCharacter + "," + result


        f.write(classifiedCharacter + "," + result + "\n")

        #cv2.imshow('output',drawing)

        #key = cv2.waitKey(0)

    cv2.imshow('output',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    f.close()
    print "classification done -- generated map."

#!/usr/bin/env python
'''
Detects characters inside circles in an image, resizes down, 
     samples 100 pixel values, and waits for keyboard input to save features
'''
import cv2
import cv2.cv as cv
import numpy as np
import time
import sys


if __name__ == '__main__':
    print "Starting training..."

    #read in image, convert to grayscale
    img = cv2.imread("../puzzles/puzzle.jpg")
    cimg = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)


    ##set up arrays for training
    samples =  np.empty((0,100))
    responses = []
    keys = [i for i in range(96,123)]


    #get circles detected in image
    circles = cv2.HoughCircles(cimg,cv2.cv.CV_HOUGH_GRADIENT,1,1,param1=200,param2=90,minRadius=0,maxRadius=0)

    
    circles = np.uint16(np.around(circles))

    loopCnt = 0
    for i in circles[0,:]:
        loopCnt += 1

        # draw a red circle on the original drawing
        cv2.circle(img,(i[0],i[1]),i[2],(0,0,255),2)

        x = i[0]
        y = i[1]
        radius = i[2]

        # print x
        # print y
        # print radius
      
    	# draw the center of the circle
        #cv2.circle(cimg,(i[0],i[1]),2,(255,255,255),3)

        #crop inside of circle and invert for better vision
        drawing = 255 - cimg[y-45:y+45,x-45:x+45]

        drawingsmall = cv2.resize(drawing,(10,10))
       
        cv2.imshow('input',img)
        cv2.imshow('output',drawing)

        key = cv2.waitKey(0)
        print chr(key) + "," + str(loopCnt)
        if key == 27:  # (escape to quit)
            sys.exit()
        elif key in keys:
            responses.append(int(key))
            sample = drawingsmall.reshape((1,100))
            samples = np.append(samples,sample,0)


	cv2.destroyAllWindows()
    responses = np.array(responses,np.float32)
    responses = responses.reshape((responses.size,1))
    print "training complete"

    np.savetxt('../trainingData/generalsamples.data',samples)
    np.savetxt('../trainingData/generalresponses.data',responses)

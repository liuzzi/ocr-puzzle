import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by frankie on 11/29/14.
 */
public class FindWordsInHexWithVision {
	public static void main(String[] args) throws Exception{

		long start = System.currentTimeMillis();
		//first load in dict words
		ArrayList<String> words = new ArrayList<>();

		int finalWordCnt = 0;

		BufferedReader reader = new BufferedReader(new FileReader(new File("mac_resultDict.txt")));
		String line = null;

		while ((line = reader.readLine()) != null){
			words.add(line);
		}
		reader.close();

		//load graph map
		HashMap<String, String> graphMap = new HashMap<>();
		reader = new BufferedReader(new FileReader(new File("graph_map.txt")));

		while ((line = reader.readLine()) != null){
			String[] lineSplit = line.split(",");
			graphMap.put(lineSplit[0],lineSplit[1]);
		}
		reader.close();


		//load graph to HashMap
		HashMap<String, HashSet<String>> graph = new HashMap<>();
		reader = new BufferedReader(new FileReader(new File("graph.txt")));

		while ((line = reader.readLine()) != null){
			line = line.toLowerCase();
			String[] lineSplit = line.split("\t");
			String startNode = lineSplit[0];

			String[] connectedNodes = lineSplit[1].split(",");

			HashSet<String> connectedNodesList = new HashSet<>();
			connectedNodesList.add(graphMap.get(startNode));

			for(String node : connectedNodes){
				connectedNodesList.add(graphMap.get(node));
			}

			graph.put(graphMap.get(startNode),connectedNodesList);
		}
		reader.close();
		System.out.println("\n--------------------------------");

		//iterate through every word, and check if it could be found by iterating the graph from any node
		for(String word : words){

			boolean validWord = true;

			for(int i = 0; i < word.length(); i++){
				if(i < word.length()-1){  //if theres a next letter, check that node path exists
					String first = Character.toString(word.charAt(i));



					String second = Character.toString(word.charAt(i + 1));

					if(graph.get(first) == null) {
						continue; // if there is a word in result dict without right letter
					}
					HashSet<String> possibleNodes = graph.get(first);
					if(!possibleNodes.contains(second)) {
						validWord = false;
						break;
					}
				}
			}
			if(validWord) {
				System.out.println(word);
				finalWordCnt++;
			}
		}

		System.out.println("--------------------------\n" +finalWordCnt +  " words found in " + (System.currentTimeMillis() - start) + "ms");

	}
}

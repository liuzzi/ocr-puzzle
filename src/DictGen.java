import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by frankie on 11/24/14.
 */
public class DictGen {
	final static Pattern p1 = Pattern.compile("(.)\\1{1}");
	final static Pattern p2 = Pattern.compile("^[brytleconwgsh]*$");

	public static void main(String[] args) throws Exception{
		BufferedReader reader = new BufferedReader(new FileReader(new File("mac_dict.txt")));
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File("mac_resultDict.txt")));
		String line = null;

		while ((line = reader.readLine()) != null){
			Matcher matcher = p1.matcher(line);
			Matcher letter = p2.matcher(line);

			if(line.length() == 6 && matcher.find() && letter.find()){

				writer.write(line);
				writer.newLine();
			}
		}

		reader.close();
		writer.close();
		System.out.println("DONE");
	}
}
